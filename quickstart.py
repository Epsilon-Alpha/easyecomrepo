from __future__ import print_function
import datetime
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from flask import Flask, request, render_template, url_for, redirect
from markupsafe import escape

#Initialize Flask
app = Flask(__name__)

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/calendar']

#Function to view last n events
@app.route("/view/<int:n>")
def view(n):
    creds = None
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)
    service = build('calendar', 'v3', credentials=creds)

    # Call the Calendar API
    now = datetime.datetime.utcnow().isoformat() + 'Z' # 'Z' indicates UTC time
    
    events_result = service.events().list(calendarId='primary', timeMin=now,
                                        maxResults=n, singleEvents=True,
                                        orderBy='startTime').execute()
    events = events_result.get('items', [])
    
    eventString = str()
    if not events:
        eventString += 'No upcoming events found.'
        return eventString
    elif len(events) < n:
        eventString += 'Only %d event(s) found.' % (len(events))
    else:
        eventString += 'Getting the upcoming %d events' % n

    eventString += '''<style>
                        table, th, td{
                            border: 1px solid black;
                            border-collapse: collapse;
                            padding: 5px;
                        }
                      </style>
                    '''
    eventString += "<table><br><tr><th>Time</th><th>Text</th></tr>"
    
    for event in events:
        start = event['start'].get('dateTime', event['start'].get('date'))
        eventString += "<tr><td>" + start + "</td><td>" + event['summary'] + "</td></tr>"
        print(start, event['summary'])
    eventString += "</table>"
    return eventString

#Function to create a simple event with an event title only
@app.route("/create/<eventText>")
def createEvent(eventText):
    creds = None
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)
    service = build('calendar', 'v3', credentials=creds)

    created_event = service.events().quickAdd(
    calendarId='primary',
    text=eventText).execute()

    resultString = str()
    if(created_event['id']):
        resultString += ("Event successfully created!") + "<br>"
        resultString += ("Event ID = %s" % (created_event['id'])) + "<br>"
        resultString += ("Event title = %s" % (created_event['summary'])) + "<br>"
        resultString += ("Event start time = %s" % (created_event['start']['dateTime'])) + "<br>"
        resultString += ("Event end time = %s" % (created_event['end']['dateTime'])) + "<br>"
    return resultString

if __name__ == '__main__':
    app.run()
