import os
import pandas as pd
from flask import Flask, request, render_template, url_for, redirect

#Initialize Flask
app = Flask(__name__)

#Route for homepage
@app.route("/")
def home():
    return render_template("upload.html")

#Route for uploading files with POST request
@app.route("/handleUpload", methods=['POST'])
def handleFileUpload():
    if request.method == 'POST':
        if 'csvdocument[]' not in request.files:
            return redirect(request.url)

        #Get the list of files
        uploadedFiles = request.files.getlist('csvdocument[]')

        for fileName in uploadedFiles:
            name = fileName.filename
            
            #Only CSV File uploads allowed
            if name[-3:]=='csv':
                filePath = os.path.join('uploads', name)
                print(filePath)
                if not os.path.exists(filePath):
                    fileName.save(filePath)
                else:
                    print(name + " file already exists")
        return redirect(url_for('displayCSV'))

#Route for data aggregation and display
@app.route("/display")
def displayCSV():
    df = pd.DataFrame()
    currDir = os.getcwd()
    uploadsDir = os.path.join(currDir,'uploads')
    allfiles = ""

    #Iterate through all files in the folder and combine in one dataframe
    for filename in os.listdir(uploadsDir):
        if '.csv' in filename:
            filepath = os.path.join(uploadsDir, filename)
            currDF = pd.read_csv(filepath)
            df = df.append(currDF)

    #Process a result
    result = pd.DataFrame()
    result['Order Number'] = df['OrderNum']
    result['Profit/loss(%)'] = df['Transferred Amount'] - df['Cost Price']
    result['Transferred Amount'] = df['Transferred Amount']
    result['Total Marketplace Charges'] = df['Commission'] + df['Payment Gateway'] + df['PickPack Fee']

    print(result)
    return result.to_html(index=False)
        
if __name__ == '__main__':
    app.run()
